const express = require('express')
const router = express.Router()
const crudController = require('../controller/crudController')
const verifyToken = require('../routes/veriyToken')


router.route('/')
    .get(verifyToken ,async (req,res) => {
        try{
            const crudSchemas = await crudSchema.find()
            res.json(crudSchemas)
        }catch(err){
            res.json({message: err})
        }
    },)
    .post(crudController.post)
    router.put('/:crudId', crudController.put)
    router.delete('/:crudId', crudController.delete)



module.exports = router
const express = require('express')
const router = express.Router()
const userModel = require('../model/userModel')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

//import validation
const {registerValidation} = require('../config/validation')



// REGISTER
router.post('/register', async (req,res) => {

    const {error} = registerValidation(req.body)
    if(error) return res.status(400).json({
        status: res.statusCode,
        message: error.details[0].message
    })

    //IF EMAIL EXIST
    const emailExist = await userModel.findOne({email: req.body.email})
    if(emailExist) return res.status(400).json({
        status: res.statusCode,
        message: 'Email Sudah digunakan'
    })
    
    //HASH PASSWORD
    const salt = await bcrypt.genSalt(10)
    const hashPassword = await bcrypt.hash(req.body.password, salt)

    const user = new userModel({
        nama: req.body.nama,
        email: req.body.email,
        password: hashPassword
    })

    try{
        const saveUser = await user.save()
        res.json(saveUser)
    }catch(err){
        res.status(400).json({
            status: res.statusCode,
            message: 'Gagal Membuat user Baru'
        })
    }
})

// LOGIN
router.post('/login', async (req,res) => {
    //if email exist
    const user = await userModel.findOne({email: req.body.email})
    if(!user) return res.status(400).json({
        status: res.statusCode,
        message: "Email Anda Salah"
    })

    //check password
    const validPwd = await bcrypt.compare(req.body.password, user.password)
    if(!validPwd) return res.status(400).json({
        status: res.statusCode,
        message: "Password anda salah"
    })

    // MEMBUAT TOKEN MENGGUNAKAN JWT
    const token = jwt.sign({_id: user._id}, process.env.SECRET_KEY)
    res.header('auth-token', token).json({
        token
    })

})

module.exports = router
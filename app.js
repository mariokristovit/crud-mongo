const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv/config')


//SETTINGAN YANG DIBUTUHKAN UNTUK MENGAKTIFKAN JSON BODY
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(cors())



//import routes
const crudRoutes = require('./routes/crudRouter')
const userRoutes = require('./routes/authRouter')
const router = require('./routes/crudRouter')
//routes.example
app.use('/api/crud', crudRoutes)
app.use('/api/user', userRoutes)

//routes.examples
app.use(crudRoutes)

//connect to db
mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error!'));
db.once('open', function () {
    console.log("we are connected")
});

// listen
app.listen(process.env.PORT, () => {
    console.log(`Server running in ${process.env.PORT}`)
})
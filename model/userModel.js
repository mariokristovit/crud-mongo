const mongoose = require('mongoose')
const userModel = mongoose.Schema({
    nama:{
        type: String,
        required: true,
        max: 255
    },
    email: {
        type: String,
        required: true,
        max: 100
    },
    password: {
        type: String,
        required: true,
        min:6,
        max: 1024
    },
    createAt:{
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('User', userModel)
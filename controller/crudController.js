const crudSchema = require('../model/crudSchema')


module.exports ={
    // get: async (req,res) => {
    //     try{
    //         const crudSchemas = await crudSchema.find()
    //         res.json(crudSchemas)
    //     }catch(err){
    //         res.json({message: err})
    //     }
    // },
    post: async (req, res) => {
        const crudPost = new crudSchema({
            nama: req.body.nama,
            alamat: req.body.alamat
        })

        try{
            const crud = await crudPost.save()
            res.json(crud)
        }catch(err){
            res.json({message: err})
        }
    },
    put: async (req, res)=> {
        try{
            const crudUpdate = await crudSchema.updateOne({_id: req.params.crudId},{
                nama: req.body.nama,
                alamat: req.body.alamat
            })
            res.json(crudUpdate)
        }catch(err){
            res.json({message: err})
        }
    },
    delete: async (req, res)=> {
        try{
            const crudUpdate = await crudSchema.deleteOne({_id: req.params.crudId})
            res.json(crudUpdate)
        }catch(err){
            res.json({message: err})
        }
    }
}